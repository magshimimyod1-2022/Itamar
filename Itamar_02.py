
from datetime import datetime, timedelta
import socket
SERVER_IP = '34.218.16.79'
SERVER_PORT = 77
"""the func gets list with the dates and calc and return the cheacksoe of the city and dates """
def checksums(calendar,city):
    first_part_in_checksum = 0
    counter = 0
    city=str(city)
    city=list(city)
    first_part_in_checksum = 0
    alphabet = {"a":1, "b":2, "c":3, "d":4, "e":5, "f":6, "g":7, "h":8, "i":9, "j":10, "k":11, "l":12, "m":13, "n":14, "o":15, "p":16, "q":17, "r":18, "s":19, "t":20, "u":21, "v":22, "w":23, "x":24, "y":25, "z":26}

    for letter in city:
        first_part_in_checksum += alphabet[letter]
    first_part_in_checksum = str(first_part_in_checksum)
    for i in calendar:
        formula = 0
        
        formula = int(i[0]) + int(i[1]) + int(i[3]) + int(i[4]) + int(i[6]) + int(i[7]) + int(i[8]) + int(i[9])
        formula="." + str(formula)
        if counter == 0 :
            checksum_today = first_part_in_checksum + formula
        elif counter == 1 :    
            checksum_tomorrow =  first_part_in_checksum + formula
        elif counter == 2 :
            checksum_day_after_tomorrow = first_part_in_checksum +  formula 
        elif counter == 3:
            checksum_two_days_after_tomorrow =  first_part_in_checksum + formula
        counter += 1
    return checksum_today, checksum_tomorrow, checksum_day_after_tomorrow, checksum_two_days_after_tomorrow

"""the func is calc and return the datetime of ,today ,tomorrow, the day after tomorrow and two days after tomorrow"""
def days():
    today = datetime.now() 
    tomorrow = today + timedelta(1)
    day_after_tomorrow = tomorrow + timedelta(1)
    two_days_after_tomorrow = day_after_tomorrow + timedelta(1)

    today = today.strftime('%d/%m/%Y')
    tomorrow =  tomorrow.strftime('%d/%m/%Y')
    day_after_tomorrow = day_after_tomorrow.strftime('%d/%m/%Y')
    two_days_after_tomorrow = two_days_after_tomorrow.strftime('%d/%m/%Y')
    return today, tomorrow, day_after_tomorrow, two_days_after_tomorrow
"""the func gets the date the city checksum and the opsion and retuns the weather, description of the weather  at that date at that city and if the option is 2 also the date """
def weather(date, city, checksum, option):
    date = str(date)
    city = str(city)
    city ="city=" + city
    checksum = 'checksum=' + checksum
    msg = "100:REQUEST:" + city + "&" + "date=" + date + "&" + checksum
    with socket.socket() as sock:
        server_address = (SERVER_IP, SERVER_PORT)
        sock.connect(server_address)
        sock.recv(4096)
        sock.send(msg.encode())
        recv_msg=sock.recv(100000)
        extractsions = recv_msg.decode()
    if '500:' in extractsions:
        return(extractsions)
    else :
        extractsions = extractsions.split('&')
    #-------------------------------------retriving the temp---------------------------------------
        temp = extractsions[2]
        temp = temp.split('=')
        temp = float (temp[1])                                                                   
    #-------------------------------------retriving the temp description---------------------------
        temp_description = extractsions[3]
        temp_description = temp_description.split('=')
        temp_description = temp_description[1]
        temp_description = temp_description.title()

    #-----------------------------------------------------------------
    
        if  option == 2:
            temperature_and_description = (date, "Temperature:" + str(temp), temp_description)

            temperature_and_description = ",".join(temperature_and_description)   
        elif option==1:
            temperature_and_description = (temp_description, temp) 

        return temperature_and_description
def main():
    today, tomorrow, day_after_tomorrow, two_days_after_tomorrow  = days()
    calendar = [today, tomorrow, day_after_tomorrow, two_days_after_tomorrow]
    # checksum_today, checksum_tomorrow,checksum_day_after_tomorrow, checksum_two_days_after_tomorrow = checksums(calendar)
    city = input("enter city: ")
    city=city.lower()
    option=int(input("enter option"))
    checksum_today, checksum_tomorrow,checksum_day_after_tomorrow, checksum_two_days_after_tomorrow = checksums(calendar,city)

    if option==1:
        today = weather(today, city, checksum_today, option)
        print(today)
    elif option == 2:
        today = weather(today, city, checksum_today, option)
        tomorrow = weather(tomorrow, city, checksum_tomorrow, option)
        day_after_tomorrow = weather(day_after_tomorrow, city, checksum_day_after_tomorrow, option)
        two_days_after_tomorrow = weather(two_days_after_tomorrow, city, checksum_two_days_after_tomorrow, option )
        print(today)
        print(tomorrow)
        print(day_after_tomorrow)
        print(two_days_after_tomorrow)


if __name__ == '__main__':
    main() 